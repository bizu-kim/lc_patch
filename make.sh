#!/bin/bash

# make patch directory
#mkdir logcenter_patch_${version}

# make docker image files
#docker save -o=logcenter_patch_${version}/images.tar $(cat ${version})

# make version
#grep ${version} | awk ~ > logcenter_patch_${version}/version

# cp elastic patch file
#cp -rRP ${version}_elastic_patch.sh logcenter_patch_${version}/elastic_patch.sh
#cp -rRP ${version}_elastic_restore.sh logcenter_patch_${version}/elastic_restore.sh

# tar patch file
#tar -czf logcenter_patch_${version}.tar.gz logcenter_patch_${version}

# done?